<?php
/**
 * Core functions loaded here
 *
 * @package blockshop
 */

?>
<?php
require_once get_template_directory() . '/core/init.php';

function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

function remove_ver_cj( $src ) {
	if( strpos( $src, '?ver=' ) )
	$src = remove_query_arg( 'ver', $src );
	return $src;
}
add_filter( 'style_loader_src', 'remove_ver_cj', 10, 2 );
add_filter( 'script_loader_src', 'remove_ver_cj', 10, 2 );

add_filter( 'woocommerce_currencies', 'add_my_currency' );
function add_my_currency( $currencies ) {
     $currencies['UAH'] = __( 'Українська гривня', 'woocommerce' );
     return $currencies;
}
add_filter('woocommerce_currency_symbol', 'add_my_currency_symbol', 10, 2);
function add_my_currency_symbol( $currency_symbol, $currency ) {
     switch( $currency ) {
         case 'UAH': $currency_symbol = 'ґрн'; break;
     }
     return $currency_symbol;
}

add_filter( 'woocommerce_get_price_html', 'custom_price_html', 10, 2 );
function custom_price_html( $wcv_price, $product ) {
    $price = $product->get_price();
	$product_cats = wp_get_post_terms($product->id, 'product_cat', $args);
	$is_chay = 0;
	foreach ($product_cats as $cat) { if ($cat->parent == 83) { $is_chay = 1; break; } }
	if ($is_chay) {
		$price_per_10g = sprintf("%.0f", $price*10);
		return sprintf('%s за 10 ґ', '<span class="woocommerce-Price-amount amount"><bdi>'.$price_per_10g.'&nbsp;<span class="woocommerce-Price-currencySymbol">ґрн</span></bdi></span>');
	} else {
		return sprintf('%s за 1 шт', '<span class="woocommerce-Price-amount amount"><bdi>'.$price.'&nbsp;<span class="woocommerce-Price-currencySymbol">ґрн</span></bdi></span>');
	}
}

add_filter( 'woocommerce_price_trim_zeros', '__return_true' );

add_action( 'woocommerce_review_order_before_payment', 'wc_privacy_message_below_checkout_button' );
function wc_privacy_message_below_checkout_button() {
	$title = esc_html__( 'Payment methods', 'woocommerce' );
   	echo '<h3>'.$title.'</h3>';
}

remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
add_action( 'woocommerce_after_checkout_form', 'woocommerce_checkout_coupon_form' );

add_filter('woocommerce_checkout_fields','change_checkout_fields');
function change_checkout_fields($fields) {
    unset($fields['order']['order_comments']);
    return $fields;
}
add_filter( 'woocommerce_billing_fields' , 'change_billing_fields' );
function change_billing_fields( $fields ) {
        unset($fields['billing_state']);
		$fields['billing_last_name']['required'] = false;
		$fields['billing_address_1']['required'] = false;
		$fields['billing_city']['required'] = false;
		$fields['billing_postcode']['required'] = false;
        return $fields;
}


// // НЕ ПЕРЕЗВАНИВАТЬ
// add_action('woocommerce_after_order_notes', 'custom_checkout_field');
// function custom_checkout_field($checkout) {
// 	echo '<div id="checkout_nocall_field">';
// 	woocommerce_form_field('checkout_nocall_field', 
// 						   array(
// 							   'type' => 'checkbox',
// 							   'class' => array('checkout_nocall_field_row form-row-wide'),
// 							   'label' => __('не перезванивать')
// 						   ),
// 						   $checkout->get_value('checkout_nocall_field')
// 						  );
// 	echo '</div>';
// }

add_filter('woocommerce_form_field' , 'remove_checkout_optional_text', 10, 4);
function remove_checkout_optional_text($field, $key, $args, $value) {
	if (is_checkout() && !is_wc_endpoint_url()) {
		$optional = '&nbsp;<span class="optional">(' . esc_html__( 'optional', 'woocommerce' ) . ')</span>';
		$field = str_replace( $optional, '', $field );
	}
	return $field;
} 

// BITRIX24
// add_action( 'woocommerce_thankyou', 'my_custom_tracking' );
function my_custom_tracking( $order_id ) {
  // Получаем информации по заказу
  $order = wc_get_order( $order_id );
  $order_data = $order->get_data();

  // Получаем базовую информация по заказу
  $order_id = $order_data['id'];
  $order_currency = $order_data['currency'];
  $order_payment_method_title = $order_data['payment_method_title'];
  $order_shipping_totale = $order_data['shipping_total'];
  $order_total = $order_data['total'];

  $order_base_info = "<hr><strong>Общая информация по заказу</strong><br>
  ID заказа: $order_id<br>
  Валюта заказа: $order_currency<br>
  Метода оплаты: $order_payment_method_title<br>
  Стоимость доставки: $order_shipping_totale<br>
  Итого с доставкой: $order_total<br>";

  // Получаем информация по клиенту
  $order_customer_id = $order_data['customer_id'];
  $order_customer_ip_address = $order_data['customer_ip_address'];
  $order_billing_first_name = $order_data['billing']['first_name'];
  $order_billing_last_name = $order_data['billing']['last_name'];
  $order_billing_email = $order_data['billing']['email'];
  $order_billing_phone = $order_data['billing']['phone'];

  $order_client_info = "<hr><strong>Информация по клиенту</strong><br>
  ID клиента = $order_customer_id<br>
  IP адрес клиента: $order_customer_ip_address<br>
  Имя клиента: $order_billing_first_name<br>
  Фамилия клиента: $order_billing_last_name<br>
  Email клиента: $order_billing_email<br>
  Телефон клиента: $order_billing_phone<br>";

  // Получаем информацию по доставке
  $order_shipping_address_1 = $order_data['shipping']['address_1'];
  $order_shipping_address_2 = $order_data['shipping']['address_2'];
  $order_shipping_city = $order_data['shipping']['city'];
  $order_shipping_state = $order_data['shipping']['state'];
  $order_shipping_postcode = $order_data['shipping']['postcode'];
  $order_shipping_country = $order_data['shipping']['country'];

  $order_shipping_info = "<hr><strong>Информация по доставке</strong><br>
  Страна доставки: $order_shipping_state<br>
  Город доставки: $order_shipping_city<br>
  Индекс: $order_shipping_postcode<br>
  Адрес доставки 1: $order_shipping_address_1<br>
  Адрес доставки 2: $order_shipping_address_2<br>";

  // Получаем информации по товару
  $order->get_total();
  $line_items = $order->get_items();
  $product_id = "";
  foreach ( $line_items as $item ) {
    $product = $order->get_product_from_item( $item );
    $sku = $product->get_sku(); // артикул товара
	$product_id = $sku;
    $id = $product->get_id(); // id товара
    $name = $product->get_name(); // название товара
    $description = $product->get_description(); // описание товара
    $stock_quantity = $product->get_stock_quantity(); // кол-во товара на складе
    $qty = $item['qty']; // количество товара, которое заказали
    $total = $order->get_line_total( $item, true, true ); // стоимость всех товаров, которые заказали, но без учета доставки

    $product_info[] = "<hr><strong>Информация о товаре</strong><br>
    Название товара: $name<br>
    ID товара: $id<br>
    Артикул: $sku<br>
    Описание: $description<br>
    Заказали (шт.): $qty<br>
    Наличие (шт.): $stock_quantity<br>
    Сумма заказа (без учета доставки): $total;";
  }
	
  $product_base_infо = implode('<br>', $product_info);
  $subject = "Заказ с сайта № $order_id";
  // Формируем URL в переменной $queryUrl для отправки сообщений в лиды Битрикс24, где
  // указываем [ваше_название], [идентификатор_пользователя] и [код_вебхука]
  $queryUrl = 'https://luzha.bitrix24.ua/rest/1/klwn9vkpqn3w9vf0/crm.lead.add.json';
  // Формируем параметры для создания лида в переменной $queryData
  $queryData = http_build_query(array(
    'fields' => array(
      'TITLE' => $subject,
      'COMMENTS' => $order_base_info.' '.$order_client_info.' '.$order_shipping_info.' '.$product_base_infо,
	  'PRODUCT_ID' => $product_id
    ),
    'params' => array("REGISTER_SONET_EVENT" => "Y")
  ));

  // Обращаемся к Битрикс24 при помощи функции curl_exec
  $curl = curl_init();
  curl_setopt_array($curl, array(
    CURLOPT_SSL_VERIFYPEER => 0,
    CURLOPT_POST => 1,
    CURLOPT_HEADER => 0,
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => $queryUrl,
    CURLOPT_POSTFIELDS => $queryData,
  ));
  $result = curl_exec($curl);
  curl_close($curl);
  $result = json_decode($result, 1);

  if (array_key_exists('error', $result)) echo "Ошибка при сохранении лида: ".$result['error_description']."<br>";
}

add_action('wp_footer', 'qty_change_logic'); 
function qty_change_logic() { 
   if (is_product()) { 
	  global $product;
	  $price = $product->get_price();
	  $stock_qty = $product->get_stock_quantity();
	  $short_desc = $product->get_short_description();
	  $product_cats = wp_get_post_terms($product->id, 'product_cat', $args);
	  $product_attrs = $product->get_attribute('pa_skolko');
	  $is_chay = 0;
	  foreach ($product_cats as $cat) {
		  if ($cat->parent == 83) {
			  $is_chay = 1;
			  break;
		  }
	  }
      ?> 
      <script>
        jQuery(function($) {
            const price = <?php echo $price ?>,
				  stock_qty = <?php echo $stock_qty ?>,
				  is_chai = <?php echo $is_chay ?>,
				  min_qty = <?php echo ($is_chay ? 10 : 1) ?>,
				  attrs = '<?php echo $product_attrs ?>'.split(', ')
			
			let refresh_html = function(ignore_limits) {
				if (stock_qty === 0) return
                let qty = $('[name=quantity]').val()
				if (qty === undefined || qty === null || qty === '') {
					$('.woocommerce-product-details__short-description').html(<?php echo("\"<p>".$short_desc."</p><p class='price'><span class='woocommerce-Price-amount amount'><bdi></bdi></span></p>\"") ?>)
				} else {
					if (qty < min_qty && !ignore_limits) {
						$('[name=quantity]').val(min_qty)
						qty = min_qty
					}
					if (qty > stock_qty) {
						$('[name=quantity]').val(stock_qty)
						qty = stock_qty
					}
					
					const product_total = parseFloat(price * qty).toFixed(1).toString().replace('.0', '')
					
					let skolko_buttons_html = '<table class="variations" cellspacing="0"><tbody><tr style="border-bottom: none !important;"><td class="value woo-variation-items-wrapper" style="padding-bottom: 0 !important;"><ul role="radiogroup" aria-label="сколько?" class="variable-items-wrapper button-variable-wrapper" data-attribute_name="attribute_pa_skolko" data-attribute_values="[]">'
					attrs.forEach((attr) => {
						skolko_buttons_html += '<li data-wvstooltip="'+attr+'" class="variable-item button-variable-item" title="'+attr+'" data-title="'+attr+'" data-value="'+attr.replace(' ґ', '').replace('полублин ', '').replace('блин! ', '')+'" role="button" tabindex="0" onclick="setQty('+attr.replace(' ґ', '').replace('полублин ', '').replace('блин! ', '')+')"><div class="variable-item-contents"><span class="variable-item-span variable-item-span-button">'+attr+'</span></div></li>'
					})
					skolko_buttons_html += '</ul></td></tr></tbody></table>'
					
					// RENDER
					document.getElementsByClassName('woocommerce-product-details__short-description')[0].innerHTML = '<p>' + <?php echo("'".htmlspecialchars($short_desc)."'") ?> + '</p><p class="price"><span class="woocommerce-Price-amount amount"><bdi>' + product_total + ' ґрн <span style="opacity:0.3;font-size:20px;">за ' + qty + (is_chai ? ' ґ' : ' шт') + '</span></bdi></span></p>' + (attrs.length > 1 ? skolko_buttons_html : '')
					$('.woocommerce-product-details__short-description').css({ whiteSpace: 'pre-wrap' })
				}
			}
			
			window.setQty = function(val) {
				$('[name=quantity]').val(val)
				refresh_html()
			}
			$(document).ready(function() {
				$('[name=quantity]').val(min_qty)
				$("[name='quantity']").attr("step", min_qty)
				refresh_html()
			})
            $('input').change(function() {
				refresh_html()
            })
            $('[name=quantity]').on('input', function() {
				refresh_html(true)
			})
        });
        </script>
      <?php 
   } 
}

add_action('wp_ajax_update_order_review', 'update_order_review');
add_action('wp_ajax_nopriv_update_order_review', 'update_order_review');
function update_order_review() {
	$stuff_id = $_POST['stuff_id'];
	$stuff_qty = $_POST['stuff_qty'];
	WC()->cart->set_quantity( $stuff_id, $stuff_qty, true );
	WC()->cart->calculate_totals();
	woocommerce_cart_totals();
	exit;
}

add_action("template_redirect", 'redirection_function');
function redirection_function(){
    global $woocommerce;
    if( is_cart() && WC()->cart->cart_contents_count == 0){
        wp_safe_redirect( get_permalink( woocommerce_get_page_id( 'shop' ) ) );
    }
}

add_filter( 'woocommerce_cart_shipping_method_full_label', 'filter_woocommerce_cart_shipping_method_full_label', 10, 2 ); 
function filter_woocommerce_cart_shipping_method_full_label( $label, $method ) { 
   if( $method->method_id == "local_pickup" ) {
       $label = "<div class='img_wrap'><img src='https://luzha.com.ua/wp-content/uploads/2021/07/self_pickup.png'/></div>".$label."<span class='desc'>Харьков культуры 22б</span>";
   } else if( $method->method_id == "flat_rate" ) {
       $label = "<div class='img_wrap'><img src='https://luzha.com.ua/wp-content/uploads/2021/07/bike-e1625239770444.jpg'/></div>".$label."<span class='desc'>Харьков</span><span class='amount'>100 ґрн</span>";       
   } else {
       $label = "<div class='img_wrap'><img src='https://luzha.com.ua/wp-content/uploads/2021/07/np.png'/></div>".$label."<span class='desc'>1-2 дня</span>";
   }
   return $label; 
}

add_action('woocommerce_checkout_update_order_meta', 'custom_checkout_field_update_order_meta');
function custom_checkout_field_update_order_meta($order_id) {
	if (!empty($_POST['contact-method'])) {
		update_post_meta($order_id, 'способ связи', sanitize_text_field($_POST['contact-method']));
	}
}