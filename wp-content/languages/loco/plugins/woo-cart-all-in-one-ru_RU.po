msgid ""
msgstr ""
"Project-Id-Version: Cart All In One For WooCommerce\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-04-09 01:48+0000\n"
"PO-Revision-Date: 2021-06-06 19:02+0000\n"
"Last-Translator: \n"
"Language-Team: Русский\n"
"Language: ru_RU\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10 >= 2 && "
"n%10<=4 &&(n%100<10||n%100 >= 20)? 1 : 2);\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/\n"
"X-Loco-Version: 2.5.2; wp-5.7.2\n"
"X-Domain: woo-cart-all-in-one"

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:172
msgid " The sidebar cart will not work on cart page and checkout page"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:319
msgid ""
"Add product to cart without reloading on single product pages and Quick View "
"popup."
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:341
msgid "Add the products which are not applied ajax add to cart"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:402
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/frontend/frontend.php:156
msgid "Add To Cart"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:96
msgid "Add To Cart Button"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:380
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:397
msgid "Add to Cart button label"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:361
msgid "Add to Cart for variable products"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:185
msgid ""
"Adding the 3rd class/id, which allows to open Sidebar Cart content when "
"clicking"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:374
msgid ""
"After click add to cart button, a pop-up will appear allowing select "
"variations and add to cart without redirect to the single product page."
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:307
msgid "AJAX Add to Cart"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:202
msgid "Allow checkout directly on Sidebar Cart without going to checkout page"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:657
msgid "Animation Facebook 1"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:658
msgid "Animation Facebook 2"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/frontend/sidebar-cart.php:520
msgid "Apply"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1122
msgid "Apply Coupon Button Background"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1161
msgid "Apply Coupon Button Hover Background"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1181
msgid "Apply Coupon Button Hover Text Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1199
msgid "Apply Coupon Button Radius(px)"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1142
msgid "Apply Coupon Button Text Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:164
msgid "Assign page"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:497
msgid "Autoplay"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:986
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1233
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1790
msgid "Background Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1668
msgid "Best Selling Products"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:539
msgid "Border Radius For Sidebar Cart Content(px)"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:523
msgid "Bottom Left"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:524
msgid "Bottom Right"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1443
msgid "Button Background"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1387
msgid "Button Enable"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1483
msgid "Button Hover Background"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1503
msgid "Button Hover Text Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1521
msgid "Button Radius(px)"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1463
msgid "Button Text Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:19
msgid "Cart All In One"
msgstr ""

#. Name of the plugin
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:18
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:84
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:465
msgid "Cart All In One For WooCommerce"
msgstr ""

#. Description of the plugin
msgid ""
"Cart All In One For WooCommerce helps your customers view cart effortlessly."
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1742
msgid "Cart button on Product Plus"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:578
msgid "Cart Effect After Add Product"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:879
msgid "Cart Icon Background"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:897
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2128
msgid "Cart Icon Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2148
msgid "Cart Icon Hover Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:791
msgid "Cart Icon Radius(px)"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:860
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2108
msgid "Cart Icon Type"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2068
msgid "Cart page"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:708
msgid "Cart Style"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1046
msgid "Cart Title"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1315
msgid "Cart total"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:390
msgid "Change the label of the add to cart button with variable products"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2271
msgid "Checkout"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1393
msgid "Checkout "
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1425
msgid "Checkout Button Text"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:191
msgid "Checkout on Sidebar Cart"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2069
msgid "Checkout page"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2071
msgid "Choose the page redirected to when clicking on Menu Cart"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:178
msgid "Class/Id to open sidebar cart content"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:606
msgid "Click"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/frontend/sidebar-cart.php:517
msgid "Coupon code"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1099
msgid "Coupon Input Radius(px)"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2299
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2316
msgid "Custom CSS"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1644
msgid "Custom Message"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1011
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1258
msgid "Dashed"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:655
msgid "Default"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:209
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:284
msgid "Design"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:248
msgid "Display menu cart on Mobile mode"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:129
msgid "Display Sidebar Cart on Mobile"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:497
msgid "Display Sidebar Content"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:356
msgid ""
"Display the notification of adding products to cart successfully after "
"adding to cart by Ajax"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1010
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1257
msgid "Dotted"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:656
msgid "Dual Ring"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:109
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:195
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:230
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:311
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:412
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:425
msgid "Enable"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1291
msgid "Enable Applied Coupons"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:730
msgid "Enable Box Shadow"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1082
msgid "Enable Coupon"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1808
msgid "Enable Image Box Shadow"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:134
msgid "Enable sidebar cart icon"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:404
msgid "Enter you own label for the add to cart button of variable products"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:168
msgid "Ex: !is_page(array(123,41,20))"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:324
msgid "Exclude products"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:627
msgid "Fade"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:628
msgid "Flip"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:560
msgid "Fly To Cart"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1993
msgid "Font Size for Trash Icon(px)"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1276
msgid "Footer Border Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1251
msgid "Footer Border Style"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:215
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:290
msgid "Go to design"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:292
msgid "Go to design Menu Cart"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1029
msgid "Header Border Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1004
msgid "Header Border Style "
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:654
msgid "Hidden"
msgstr ""

#. Author URI of the plugin
msgid "https://villatheme.com"
msgstr ""

#. URI of the plugin
msgid "https://villatheme.com/extensions/woocommerce-cart-all-in-one/"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:608
msgid ""
"If choose \"Click\", the cart content will be shown after clicking on the "
"cart icon"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:479
msgid "Infinite loop"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:661
msgid "Loader Balls 1"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:662
msgid "Loader Balls 2"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:663
msgid "Loader Balls 3"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:681
msgid "Loading Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:650
msgid "Loading Type"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:95
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2048
msgid "Menu Cart"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2189
msgid "Menu Cart Price"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2166
msgid "Menu Cart Text"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:267
msgid "Menus"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:121
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:241
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:434
msgid "Mobile enable"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:605
msgid "MouseOver"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1850
msgid "Name Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1868
msgid "Name Hover Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2063
msgid "Navigation Page"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1008
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1255
msgid "No border"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/frontend/sidebar-cart.php:714
msgid "No products in the cart."
msgstr "пуст."

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:582
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1667
msgid "None"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:347
msgid "Notification"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:488
msgid "Number of carousel items that should move on animation"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:470
msgid "Number of columns"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1721
msgid "Number Of Products To Show"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:583
msgid "Open cart"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:515
msgid "Pause on hover"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/woo-cart-all-in-one.php:33
msgid ""
"Please install and activate WooCommerce to use Cart All In One For "
"WooCommerce."
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/frontend/ajax-add-to-cart.php:45
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/frontend/variable-atc.php:66
msgid ""
"Please select some product options before adding this product to your cart."
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/woo-cart-all-in-one.php:30
msgid ""
"Please update PHP version at least 7.0 to use Cart All In One For "
"WooCommerce."
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:443
msgid "Position on single product page"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2172
msgid "Price"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1369
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1888
msgid "Price Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1906
msgid "Price Style"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1309
msgid "Price to display"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2171
msgid "Product Counter"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2173
msgid "Product Counter & Price"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:916
msgid "Product Counter Background Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:951
msgid "Product Counter Border Radius(px)"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:935
msgid "Product Counter Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1826
msgid "Product Image Border Radius(px)"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1687
msgid "Product Plus Title"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1704
msgid "Product Plus Title Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1910
msgid "Product price"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/frontend/sidebar-cart.php:824
msgctxt "Product quantity input tooltip"
msgid "Qty"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1912
msgid "Product subtotal"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:461
msgid "Products limit"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1911
msgid "Qty & price"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1930
msgid "Quantity Border Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1948
msgid "Quantity Border Radius(px)"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/frontend/menu-cart.php:35
msgid "Quick checkout"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:97
msgid "Recently Viewed Products"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1669
msgid "Recently Viewed products"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/frontend/sidebar-cart.php:746
msgid "Remove this item"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:659
msgid "Ring"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:664
msgid "Ripple"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:630
msgid "Roll"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:660
msgid "Roller"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:631
msgid "Rotate"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:632
msgid "RotateInDown"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:633
msgid "RotateInUp"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:526
msgid "Save"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:279
msgid ""
"Select menus to display the menu Cart.  Clicking on save button before \"Go "
"to Design\""
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:365
msgid "Select variation pop-up"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:170
msgid "Set pages to display the sidebar cart using"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:755
msgid ""
"Set the sidebar cart icon size. This new size parameter need to be the a "
"ratio compared with original icon size"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:777
msgid "Set the size of Sidebar Cart Icon when hovering"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/admin.php:19
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/admin.php:20
msgid "Settings"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:584
msgid "Shake Horizontal"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:585
msgid "Shake Vertical"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2086
msgid "Show Content Cart"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:261
msgid "Show Menu Cart cart even when it is empty"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1662
msgid "Show Products Plus"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:157
msgid "Show Sidebar cart even when it is empty"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:141
msgid "Show Sidebar Cart icon on your site"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:94
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:483
msgid "Sidebar Cart"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1216
msgid "Sidebar Cart Footer"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:969
msgid "Sidebar Cart Header"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:813
msgid "Sidebar Cart Horizontal(px)"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:694
msgid "Sidebar Cart Icon"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:747
msgid "Sidebar Cart Icon Size"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:769
msgid "Sidebar Cart Icon Size When Hovering"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1756
msgid "Sidebar Cart List Products"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:517
msgid "Sidebar Cart Position"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:834
msgid "Sidebar Cart Vertical(px)"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:623
msgid "Sidebar Trigger Event Style"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:601
msgid "Sidebar Trigger Event Type"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:629
msgid "Slide"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:506
msgid "Slideshow speed(milliseconds)"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1009
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1256
msgid "Solid"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/frontend/ajax-add-to-cart.php:46
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/frontend/variable-atc.php:67
msgid ""
"Sorry, this product is unavailable. Please choose a different combination."
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:665
msgid "Spinner"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2245
msgid "Sticky Add To Cart Button"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:408
msgid "Sticky Add To Cart on single product page"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:716
msgid "Style four"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:501
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:713
msgid "Style one"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:715
msgid "Style three"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:502
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:714
msgid "Style two"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2195
msgid "Subtotal"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1314
msgid "Subtotal (total of products)"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2213
msgid "Text Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2231
msgid "Text Color Hover"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1729
msgid "The maximum number of  showed products is 15"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:563
msgid "The products will be flown to Cart after clicking on add to cart button"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1688
msgid "The title of suggested products list in footer"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:452
msgid "Title"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1065
msgid "Title Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:521
msgid "Top Left"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1670
msgid "Top Rated Products"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:522
msgid "Top Right"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2194
msgid "Total"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1350
msgid "Total Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1331
msgid "Total Text"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2017
msgid "Trash Icon Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:2035
msgid "Trash Icon Hover Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1976
msgid "Trash Icon Style"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/customize-control.php:22
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/customize-control.php:27
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:138
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:182
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:199
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:416
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:429
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:438
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:447
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:456
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:465
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:474
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:483
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:492
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:501
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:510
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:519
msgid "Unlock This Feature"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1546
msgid "Update Button Background"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1586
msgid "Update Button Hover Background"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1605
msgid "Update Button Hover Text Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1623
msgid "Update Button Radius(px)"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1566
msgid "Update Button Text Color"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/frontend/sidebar-cart.php:569
#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/frontend/sidebar-cart.php:593
msgid "Update Cart"
msgstr "обновить"

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1770
msgid "Update cart when changing the product quantity"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1392
msgid "View cart "
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:1409
msgid "View Cart Button Text"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/frontend/menu-cart.php:35
msgid "View your shopping cart"
msgstr ""

#. Author of the plugin
msgid "VillaTheme"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:253
msgid "Visible empty menu cart"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:148
msgid "Visible empty sidebar cart icon"
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/cart.php:171
msgid "WP's conditional tags."
msgstr ""

#: D:/git/free-version/woo-cart-all-in-one/woo-cart-all-in-one/includes/admin/design.php:634
msgid "Zoom"
msgstr ""
