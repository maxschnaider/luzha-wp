��          �       �      �     �  M   �     �     �       '        A     M  N   h     �     �     �     �  7   �  B   #  <   f  ;   �     �     �  ,   �  2        Q  A  m     �  s   �     ?     N  1   f  @   �     �  6   �  �   !     �  !   �     �     	  }   	  }   �	  ~   
  t   �
            ?   -  2   m     �   API Key is incorrect Accept Bitcoin Payments on your WooCommerce-powered website with Blockonomics Bitcoin Blockonomics Copied to clipboard Customer cancelled blockonomics payment Description Enable Blockonomics plugin Extra Currency Rate Margin % (Increase live fiat to BTC rate by small percent) Order # Pay with bitcoin Payment completed Settings Show bitcoin as an option to customers during checkout? This controls the description which the user sees during checkout. This controls the title which the user sees during checkout. Time period of countdown timer on payment page (in minutes) Title Transaction Your server is blocking outgoing HTTPS calls https://github.com/blockonomics/woocommerce-plugin https://www.blockonomics.co PO-Revision-Date: 2021-05-19 23:41+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n % 10 == 1 && n % 100 != 11) ? 0 : ((n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 12 || n % 100 > 14)) ? 1 : 2);
X-Generator: Loco https://localise.biz/
Language: ru_RU
Project-Id-Version: Plugins - WordPress Bitcoin Payments &#8211; Blockonomics - Stable (latest release)
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2021-05-19 23:38+0000
Last-Translator: 
Language-Team: Русский
X-Loco-Version: 2.5.2; wp-5.7.2 API ключ неверен Принимате биткоин улажены на вашем WooCommerce сайте с Блокономикой Биткоин Блокономика  Скопировано в буфер обмена Клиент отменил платеж блокономики  Описание Запустить плагин Блокономики Дополнительная валютная маржа % (Увеличьте ставку фиата к btc на небольшой процент) Заказ # Подтвердить заказ Платеж завершен Настройки Показывать клиентам биткоин как вариант во время оформления заказа? Это контролирует описание, которое юзер видит при оформлении заказа Это контролирует название, которое юзер видит при оформлении заказа. Время таймера обратного отсчета на странице платежа (в минутах) Название Транзакция Ваш сервер блокирует исходящие HTTPS https://github.com/blockonomics/woocommerce-plugin https://www.blockonomics.co 